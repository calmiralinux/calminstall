build:
	cargo build

build.release:
	cargo build --release
	strip -s target/release/calminstall

install:
	cp target/release/calminstall /sbin/calminstall

clean:
	cargo clean
