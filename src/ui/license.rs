use std::fs;

use cursive::view::Scrollable;
use cursive::views::{Dialog, LinearLayout, Panel, TextView};
use cursive::Cursive;

use crate::constants::LICENSE_PATH;
use crate::ui::poweroff;

pub fn show_license_window(screen: &mut Cursive) {
    let license = match fs::read_to_string(LICENSE_PATH) {
        Ok(license) => license,
        Err(why) => {
            log::warn!("Can't read {}: {}", LICENSE_PATH, why);
            String::new()
        }
    };

    let window_text = LinearLayout::vertical()
        .child(TextView::new(
            "All developments of the \"Calmira GNU/Linux-libre\" project are distributed\n\
                 under the GNU GPLv3 license. Be sure to read the terms of this license.",
        ))
        .child(Panel::new(TextView::new(license).scrollable()));

    let license_window = Dialog::around(window_text)
        .title("License")
        .button("Accept", on_accept)
        .button("Decline", poweroff::poweroff_window);

    screen.add_layer(license_window);
}

fn on_accept(screen: &mut Cursive) {
    screen.pop_layer();

    super::hostname::show_hostname_window(screen);
}
