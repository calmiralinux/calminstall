use anyhow::{anyhow, Result};
use cursive::view::{Nameable, Scrollable};
use cursive::views::{
    Checkbox, Dialog, EditView, LinearLayout, ListView, Panel, SelectView, TextView,
};
use cursive::Cursive;

use crate::settings::{AccountType, Settings, UsersSettings};
use crate::utils::{calculate_id, get_shell_index, get_shells};

use super::error::show_info_window;

/// Create new uaser with default values and add it to the Settings.
/// Calculate next empty UID and GID.
pub fn new_user(screen: &mut Cursive) {
    let mut user = UsersSettings::default();

    // Assign new uid
    let current_uids = screen
        .with_user_data(|settings: &mut Settings| settings.get_all_uids())
        .unwrap_or_default();
    user.uid = Some(calculate_id(&current_uids));

    // Assign new gid
    let current_gids = screen
        .with_user_data(|settings: &mut Settings| settings.get_all_gids())
        .unwrap_or_default();
    user.gid = Some(calculate_id(&current_gids));

    show_user_dialog(screen, &user);
}

/// Show last user for editing
pub fn edit_user(screen: &mut Cursive) {
    let user = screen
        .with_user_data(|settings: &mut Settings| settings.users.pop().unwrap_or_default())
        .unwrap_or_default();

    show_user_dialog(screen, &user);
}

/// Shows window to fill information about user.
/// This window will read (and remove) the last user from the list of users added by the
/// 'new_user' function.
pub fn show_user_dialog(screen: &mut Cursive, user: &UsersSettings) {
    screen.pop_layer();

    let text = TextView::new(
        "Please fill in the information for the new\n\
         non-privileged non-system user.\n\n\
         Parameters marked as [*] are required!",
    );

    let giduid = ListView::new()
        .child(
            "Custom User ID:",
            EditView::new()
                .content(user.uid.as_ref().unwrap_or(&String::new()))
                .disabled()
                .with_name("custom_user_id"),
        )
        .child(
            "Custom Group ID:",
            EditView::new()
                .content(user.gid.as_ref().unwrap_or(&String::new()))
                .disabled()
                .with_name("custom_group_id"),
        );

    // Create list of available shells and select used by user.
    // If user has not supported shell select first from the list.
    let avilable_shells = get_shells();
    let index = get_shell_index(&avilable_shells, &user.login_shell).unwrap_or(0);
    log::debug!("Set shell index {}", index);
    let mut shells = SelectView::new();
    shells.add_all_str(&avilable_shells);
    shells.set_selection(index);

    let content = LinearLayout::vertical()
        .child(
            ListView::new()
                .child(
                    "User name (short) [*] :",
                    EditView::new()
                        .content(user.name.clone())
                        .on_edit(move |s, text, _cursor| {
                            s.call_on_name("home_dir", |v: &mut EditView| v.set_content(text));
                        })
                        .with_name("name_short"),
                )
                .child(
                    "User name (full):",
                    EditView::new()
                        .content(user.user_description.clone().unwrap_or_default())
                        .with_name("name_full"),
                )
                .child(
                    "Password [*] :",
                    EditView::new()
                        .content(user.password.clone())
                        .secret()
                        .with_name("passwd"),
                )
                .child(
                    "Repeat password [*] :",
                    EditView::new().secret().with_name("passwd_repeat"),
                )
                .delimiter()
                .child(
                    "Create user group",
                    Checkbox::new()
                        .with_checked(user.user_group)
                        .on_change(move |s, c| {
                            // I don't know why, but chainable variant will not work
                            s.call_on_name("custom_user_id", |v: &mut EditView| v.set_enabled(c));
                            s.call_on_name("custom_group_id", |v: &mut EditView| v.set_enabled(c));
                        })
                        .with_name("create_group"),
                ),
        )
        .child(Panel::new(giduid))
        .child(
            ListView::new()
                .delimiter()
                .child(
                    "Custom Login shell:",
                    shells.popup().with_name("login_shell"),
                )
                .child(
                    "Custom Home directory:",
                    EditView::new()
                        .content(user.home_dir.clone().unwrap_or_default())
                        .with_name("home_dir"),
                )
                .delimiter()
                .child(
                    "This is system account:",
                    Checkbox::new()
                        .with_checked(user.system_account.0)
                        .with_name("system_account"),
                ),
        );

    let layout = LinearLayout::vertical()
        .child(text)
        .child(Panel::new(content));

    let window = Dialog::around(layout.scrollable())
        .title("User and group management")
        .button("Create new user", on_create_new)
        .button("Show users", on_user_manager)
        .button("Finish creating users", on_finish);

    screen.add_layer(window);
}

fn on_finish(screen: &mut Cursive) {
    match add_user(screen) {
        Ok(_) => {
            screen.pop_layer();

            log::info!("User created successfully");

            super::timezone::new_timezone(screen);
        }
        Err(why) => {
            log::info!("{}", &why);
            screen.pop_layer();
            show_info_window(screen, &why.to_string(), edit_user);
        }
    }
}

fn on_create_new(screen: &mut Cursive) {
    match add_user(screen) {
        Ok(_) => {
            screen.pop_layer();

            log::info!("User created successfully");

            new_user(screen);
        }
        Err(why) => {
            log::info!("{}", &why);
            screen.pop_layer();
            show_info_window(screen, &why.to_string(), edit_user);
        }
    }
}

fn on_user_manager(screen: &mut Cursive) {
    match add_user(screen) {
        Ok(_) => {
            screen.pop_layer();

            log::info!("User created successfully");

            super::user_management::show_user_management_window(screen);
        }
        Err(why) => {
            log::info!("{}", &why);
            screen.pop_layer();
            show_info_window(screen, &why.to_string(), edit_user);
        }
    }
}

fn add_user(screen: &mut Cursive) -> Result<()> {
    let user_settings = get_user_settings(screen);

    // check status before struct borrowed
    let is_user_correct = user_settings.is_correct();

    screen.with_user_data(|settings: &mut Settings| {
        settings.users.push(user_settings);
    });

    if !is_user_correct {
        return Err(anyhow!("User name and password can not be empty!"));
    }

    if !is_password_match(screen) {
        return Err(anyhow!("Password not match! Please pereat password."));
    }

    Ok(())
}

fn get_user_settings(screen: &mut Cursive) -> UsersSettings {
    let user_full = screen
        .call_on_name("name_full", |v: &mut EditView| v.get_content())
        .unwrap_or_default();
    let user_short = screen
        .call_on_name("name_short", |v: &mut EditView| v.get_content())
        .unwrap_or_default();
    let passwd = screen
        .call_on_name("passwd", |v: &mut EditView| v.get_content())
        .unwrap_or_default();
    let create_group = screen
        .call_on_name("create_group", |v: &mut Checkbox| v.is_checked())
        .unwrap_or(false);
    let custom_user_id = screen
        .call_on_name("custom_user_id", |v: &mut EditView| v.get_content())
        .unwrap_or_default();
    let custom_group_id = screen
        .call_on_name("custom_group_id", |v: &mut EditView| v.get_content())
        .unwrap_or_default();
    let login_shell = screen
        .call_on_name("login_shell", |v: &mut SelectView| {
            v.selection().unwrap_or_default()
        })
        .unwrap_or_default();
    let home_dir = screen
        .call_on_name("home_dir", |v: &mut EditView| v.get_content())
        .unwrap_or_default();
    let system_account = screen
        .call_on_name("system_account", |v: &mut Checkbox| v.is_checked())
        .unwrap_or(false);

    UsersSettings {
        name: user_short.to_string(),
        user_description: to_option(&user_full),
        password: passwd.to_string(),
        user_group: create_group,
        gid: to_option(&custom_group_id),
        uid: to_option(&custom_user_id),
        login_shell: login_shell.to_string(),
        home_dir: to_option(&home_dir),
        system_account: AccountType::new(system_account),
    }
}

fn is_password_match(screen: &mut Cursive) -> bool {
    let passwd = screen
        .call_on_name("passwd", |v: &mut EditView| v.get_content())
        .unwrap_or_default();

    let passwd_repeat = screen
        .call_on_name("passwd_repeat", |v: &mut EditView| v.get_content())
        .unwrap_or_default();

    passwd == passwd_repeat
}

fn to_option(value: &str) -> Option<String> {
    if !value.is_empty() {
        Some(value.to_string())
    } else {
        None
    }
}
