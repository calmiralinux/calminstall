use cursive::views::{Dialog, TextView};
use cursive::Cursive;

use super::requirements::check_requirements;

pub fn show_welcome_window(screen: &mut Cursive) {
    let welcome = Dialog::around(TextView::new(
        "Welcome to Calmira GNU/Linux-libre installer!
Would you like to begin an installation or use the
Live CD/USB mode (try Calmira without installation)?",
    ))
    .title("Welcome to Setup")
    .button("Install", on_install)
    .button("Live mode", |s| s.quit())
    .button("Cancel", on_cancel);

    screen.add_layer(welcome);
}

fn on_install(screen: &mut Cursive) {
    log::info!("Install Calmira Linux selected.");
    check_requirements(screen);
}

fn on_cancel(screen: &mut Cursive) {
    log::info!("Installation canceled.");
    screen.pop_layer();
    super::abort::show_abort_window(screen);
}
