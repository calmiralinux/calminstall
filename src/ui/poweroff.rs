use cursive::views::{Dialog, TextView};
use cursive::Cursive;
use crate::utils;

pub fn poweroff_window(screen: &mut Cursive) {
    let window = Dialog::around(
        TextView::new("Are you sure you want to abort the installation and turn off your PC?")
    )
        .title("Shut down PC")
        .button("Shut down", |_| utils::poweroff())
        .button("Restart", |_| utils::restart())
        .button("Return to Shell", |s| s.quit());

    screen.pop_layer();
    screen.add_layer(window);
}
