use std::path::PathBuf;

use cursive::{
    align::HAlign,
    event::EventResult,
    view::{Resizable, Scrollable},
    views::{Dialog, OnEventView, Panel, SelectView},
    Cursive,
};

use crate::{
    constants::TIMEZONE_DIR,
    settings::Settings,
    utils::{scan_dir, ScanType},
};

use super::error::show_error_window;

pub fn new_timezone(screen: &mut Cursive) {
    // Clear timezone value for the first call.
    screen.with_user_data(|settings: &mut Settings| {
        if !settings.tz.is_empty() {
            settings.tz = String::new()
        }
    });

    timezone(screen);
}

fn timezone(screen: &mut Cursive) {
    let mut dir = PathBuf::from(TIMEZONE_DIR);
    let mut filter = ScanType::Dir;
    let mut title = "Select Timezone";

    let current_timezone = screen
        .with_user_data(|data: &mut Settings| data.tz.clone())
        .unwrap_or_default();

    if !current_timezone.is_empty() {
        filter = ScanType::DirAndFile;
        dir.push(current_timezone);
        title = "Select Country or Region";
    }

    match scan_dir(&dir, 1, filter) {
        Ok(files) => {
            let mut zones: Vec<&String> = files
                .iter()
                .filter(|s| s.as_str() != "Etc")
                .filter(|s| s.to_lowercase().as_str() != s.as_str())
                .collect();
            zones.sort();
            show_timezone_dir(screen, zones, title);
        }
        Err(err) => show_error_window(screen, &format!("Could not detect timezone: {}", err)),
    }
}

fn show_timezone_dir(screen: &mut Cursive, files: Vec<&String>, title: &str) {
    let mut timezone_select = SelectView::new().h_align(HAlign::Left).autojump();

    timezone_select.add_all_str(files);
    timezone_select.set_on_submit(on_selected_zone);

    let select = OnEventView::new(timezone_select)
        .on_pre_event_inner('k', |s, _| {
            let cb = s.select_up(1);
            Some(EventResult::Consumed(Some(cb)))
        })
        .on_pre_event_inner('j', |s, _| {
            let cb = s.select_down(1);
            Some(EventResult::Consumed(Some(cb)))
        });
    screen.add_layer(Dialog::around(Panel::new(select.scrollable().fixed_width(30))).title(title));
}

fn on_selected_zone(screen: &mut Cursive, tz: &str) {
    screen.pop_layer();

    let current_timezone = screen
        .with_user_data(|data: &mut Settings| data.tz.clone())
        .unwrap_or_default();

    let mut tz_as_path = PathBuf::from(current_timezone);
    tz_as_path.push(tz);
    let tz_as_string = tz_as_path.to_str().unwrap_or_default();

    screen.with_user_data(|settings: &mut Settings| settings.tz = tz_as_string.to_string());

    if is_done(screen) {
        log::info!("Current timezone: {}", &tz_as_string);

        super::confirmation::show_confirmation_window(screen);
    } else {
        timezone(screen);
    }
}

fn is_done(screen: &mut Cursive) -> bool {
    let current_timezone = screen
        .with_user_data(|data: &mut Settings| data.tz.clone())
        .unwrap_or_default();

    let mut path = PathBuf::from(TIMEZONE_DIR);
    path.push(current_timezone);
    log::debug!("Full path is {}", &path.to_string_lossy());

    path.is_file()
}
