use cursive::view::{Nameable, Resizable};
use cursive::views::{Dialog, EditView, LinearLayout, TextView};
use cursive::Cursive;

use crate::constants;
use crate::settings::Settings;

pub fn show_hostname_window(screen: &mut Cursive) {
    let current_hostname = screen
        .user_data::<Settings>()
        .unwrap_or(&mut Settings::default())
        .hostname
        .clone();

    let hostname = Dialog::new()
        .title("Set Hostname")
        .padding_lrtb(1, 1, 1, 0)
        .content(
            LinearLayout::vertical()
                .child(TextView::new("Please choose a hostname for this PC:"))
                .child(
                    EditView::new()
                        .content(current_hostname)
                        .on_submit(on_enter)
                        .with_name("hostname")
                        .fixed_width(40),
                ),
        )
        .button("Ok", |s| {
            let name = s
                .call_on_name("hostname", |view: &mut EditView| {
                    // We can return content from the closure!
                    view.get_content()
                })
                .unwrap_or_else(|| constants::DEFAULT_HOSTNAME.to_string().into());
            on_enter(s, &name);
        });

    screen.add_layer(hostname);
}

fn on_enter(screen: &mut Cursive, hostname: &str) {
    screen.pop_layer();
    screen.with_user_data(|settings: &mut Settings| {
        settings.hostname = hostname.to_owned();
    });

    log::info!("Hostname set to {} successfully.", hostname);

    super::disk::show_disk_window(screen);
}
