use std::cmp::Ordering;

use anyhow::anyhow;
use cursive::view::{Nameable, Resizable};
use cursive::views::{Dialog, LinearLayout, Panel, TextView};
use cursive::Cursive;
use cursive_table_view::{TableView, TableViewItem};

use super::users::new_user;
use crate::settings::{Settings, UsersSettings};

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
enum UserColumn {
    Name,
    Uid,
    Gid,
    AccountType,
}

impl UserColumn {
    fn as_str(&self) -> &str {
        match *self {
            UserColumn::Name => "User",
            UserColumn::Uid => "UID",
            UserColumn::Gid => "GID",
            UserColumn::AccountType => "Account Type",
        }
    }
}

impl TableViewItem<UserColumn> for UsersSettings {
    fn to_column(&self, column: UserColumn) -> String {
        match column {
            UserColumn::Name => self.name.clone(),
            UserColumn::Uid => self.uid.clone().unwrap_or_default(),
            UserColumn::Gid => self.gid.clone().unwrap_or_default(),
            UserColumn::AccountType => self.system_account.to_string(),
        }
    }

    fn cmp(&self, other: &Self, column: UserColumn) -> Ordering
    where
        Self: Sized,
    {
        match column {
            UserColumn::Name => self.name.cmp(&other.name),
            UserColumn::Uid => self.uid.cmp(&other.uid),
            UserColumn::Gid => self.gid.cmp(&other.gid),
            UserColumn::AccountType => self.system_account.0.cmp(&other.system_account.0),
        }
    }
}

pub fn user_manager(screen: &mut Cursive) {
    let users = screen
        .with_user_data(|settings: &mut Settings| settings.users.clone())
        .unwrap_or_default();
    if users.is_empty() {
        new_user(screen);
    } else {
        show_user_management_window(screen);
    }
}

pub fn show_user_management_window(screen: &mut Cursive) {
    let users = screen
        .with_user_data(|settings: &mut Settings| settings.users.clone())
        .unwrap_or_default();

    let text = TextView::new(
        "List of all users to be created. As long as you have\n\
        not allowed the installation of the system,you can change\n\
        the configureation of the users you specified.\n\n",
    );

    let mut users_table = TableView::<UsersSettings, UserColumn>::new()
        .column(UserColumn::Name, UserColumn::Name.as_str(), |c| {
            c.width(15).align(cursive::align::HAlign::Left)
        })
        .column(UserColumn::Uid, UserColumn::Uid.as_str(), |c| {
            c.ordering(Ordering::Less)
                .align(cursive::align::HAlign::Center)
                .width(8)
        })
        .column(UserColumn::Gid, UserColumn::Gid.as_str(), |c| {
            c.width(8).align(cursive::align::HAlign::Center)
        })
        .column(
            UserColumn::AccountType,
            UserColumn::AccountType.as_str(),
            |c| c.width(16).align(cursive::align::HAlign::Left),
        )
        .default_column(UserColumn::Uid)
        .selected_row(0);

    users_table.set_items(users);

    users_table.set_on_submit(|screen: &mut Cursive, _row: usize, index: usize| {
        if let Some(user) = get_selected(screen, index) {
            // Remove selected user from the list to avoid dublicating
            let result = screen
                .with_user_data(|settings: &mut Settings| settings.remove_user(&user))
                .unwrap_or_else(|| Err(anyhow!("Remove user error!")));
            match result {
                Ok(_) => super::users::show_user_dialog(screen, &user),
                Err(why) => log::warn!("{}", why.to_string()),
            }
        }
    });

    let layout = LinearLayout::vertical().child(text).child(Panel::new(
        users_table.with_name("table_users").min_height(7),
    ));

    let window = Dialog::around(layout)
        .title("User management")
        .button("Finish", on_finish)
        .button("Add", on_add)
        .button("Edit", on_edit);

    screen.add_layer(window.min_width(64));
}

fn on_finish(screen: &mut Cursive) {
    screen.pop_layer();

    super::confirmation::show_confirmation_window(screen);
}

fn on_add(screen: &mut Cursive) {
    screen.pop_layer();

    super::users::new_user(screen);
}

fn on_edit(screen: &mut Cursive) {
    if let Some(index) = screen.call_on_name(
        "table_users",
        move |table: &mut TableView<UsersSettings, UserColumn>| table.item().unwrap_or_default(),
    ) {
        if let Some(user) = get_selected(screen, index) {
            // Remove selected user from the list to avoid dublicating
            let result = screen
                .with_user_data(|settings: &mut Settings| settings.remove_user(&user))
                .unwrap_or_else(|| Err(anyhow!("Remove user error!")));
            match result {
                Ok(_) => super::users::show_user_dialog(screen, &user),
                Err(why) => log::warn!("{}", why.to_string()),
            }
        }
    }
}

/// Return struct from selected row
fn get_selected(screen: &mut Cursive, index: usize) -> Option<UsersSettings> {
    screen.call_on_name(
        "table_users",
        move |table: &mut TableView<UsersSettings, UserColumn>| {
            let default_settings = UsersSettings::default();
            let item = table.borrow_item(index).unwrap_or(&default_settings);
            item.clone()
        },
    )
}
