use std::fs;

use cursive::views::{Dialog, LinearLayout, ProgressBar, TextView};
use cursive::Cursive;

use super::error::{show_error_window, show_warning_window};
use super::license::show_license_window;
use crate::constants::{FLIST_PATH, REQUIRED_CPU, REQUIRED_MEM};
use crate::utils::{check_files, get_cpu_freq_mhz, get_memory_mb};

pub fn check_requirements(screen: &mut Cursive) {
    let cpu_freq = get_cpu_freq_mhz();
    let memory_size = get_memory_mb();

    screen.pop_layer();

    if cpu_freq == 0 || memory_size == 0 {
        show_warning_window(
            screen,
            "Unable to get information about CPU/RAM",
            on_warning_next,
        );
    } else if cpu_freq < REQUIRED_CPU && memory_size < REQUIRED_MEM {
        log::warn!("PC not meet system requirements.");
        show_error_window(screen, &format!("Your system does not meet the minimum system requirements.\n
You have CPU: {} Mhz and RAM: {} Mb.\nThe minimum system requirements are: CPU {} Mhz and RAM {} Mb",
        cpu_freq,
        memory_size,
        REQUIRED_CPU,
        REQUIRED_MEM
        ));
    } else {
        log::info!("The PC meet requirements.");
        show_check_files_window(screen);
    }
}

fn show_check_files_window(screen: &mut Cursive) {
    match fs::read_to_string(FLIST_PATH) {
        Ok(content) => {
            let files: Vec<String> = content.lines().map(|s| s.to_owned()).collect();

            // This is the callback channel
            let cb_ok = screen.cb_sink().clone();
            let cb_err = screen.cb_sink().clone();

            let progressbar = ProgressBar::new()
                .range(0, files.len())
                .with_task(move |counter| {
                    if check_files(files, &counter) {
                        cb_err.send(Box::new(on_check_err)).unwrap();
                    } else {
                        cb_ok.send(Box::new(on_check_ok)).unwrap();
                    }
                });

            let dialog = Dialog::new().title("Check required files").content(
                LinearLayout::vertical()
                    .child(TextView::new("Checking system files. Please wait..."))
                    .child(progressbar),
            );

            screen.set_autorefresh(true);
            screen.add_layer(dialog);
        }
        Err(why) => {
            log::warn!("Could not find list of files.\n{}: {}", FLIST_PATH, why);
            show_error_window(
                screen,
                &format!("Could not find list of files.\n{}: {}", FLIST_PATH, why),
            )
        }
    };
}

fn on_check_ok(screen: &mut Cursive) {
    log::info!("Check files complete.");
    screen.set_autorefresh(false);
    screen.pop_layer();
    show_license_window(screen);
}

fn on_check_err(screen: &mut Cursive) {
    screen.set_autorefresh(false);
    screen.pop_layer();
    show_error_window(
        screen,
        "Check files error.\nImpossible to continue installation.",
    );
}

fn on_warning_next(screen: &mut Cursive) {
    screen.pop_layer();
    show_check_files_window(screen);
}
