use cursive::{
    view::Scrollable,
    views::{Dialog, LinearLayout, ListView, Panel, TextView},
    Cursive,
};

use crate::settings::Settings;

pub fn show_confirmation_window(screen: &mut Cursive) {
    let mut default_settings = Settings::default();
    let settings = screen
        .user_data::<Settings>()
        .unwrap_or(&mut default_settings);

    let text = TextView::new("Please check your configuration before continue.\n");

    let common = ListView::new()
        .child("Hostname:", TextView::new(settings.hostname.clone()))
        .child("Disk: ", TextView::new(settings.disk.clone()))
        .child("Timezone: ", TextView::new(settings.tz.clone()));

    let mut users = LinearLayout::vertical();

    for user in settings.users.clone() {
        users.add_child(Panel::new(TextView::new(user.to_string())))
    }

    let layout = LinearLayout::vertical()
        .child(text)
        .child(TextView::new("System:"))
        .child(Panel::new(common))
        .child(TextView::new("Users:"))
        .child(users.scrollable());

    let window = Dialog::around(layout)
        .title("Check settings")
        .button("Continue", on_continue)
        .button("Back", on_back);
    screen.add_layer(window);
}

fn on_continue(screen: &mut Cursive) {
    screen.pop_layer();

    // TODO: Not implemented
    super::welcome::show_welcome_window(screen);
}

fn on_back(screen: &mut Cursive) {
    screen.pop_layer();

    super::welcome::show_welcome_window(screen);
}
