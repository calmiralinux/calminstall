
use cursive::views::{Dialog, TextView};
use cursive::Cursive;

use crate::ui::poweroff;

pub fn show_error_window(screen: &mut Cursive, msg: &str) {
    let abort = Dialog::around(TextView::new(msg))
    .title("Error")
    .button("Shut Down", poweroff::poweroff_window)
    .button("Exit", |s| s.quit());

    screen.add_layer(abort);
}

pub fn show_warning_window<F>(screen: &mut Cursive, msg: &str, cb_next: F)
where
    F: 'static + Fn(&mut Cursive),
{
    let abort = Dialog::around(TextView::new(msg))
    .title("Warning")
    .button("Shut Down", poweroff::poweroff_window)
    .button("Exit", |s| s.quit())
    .button("Continue", cb_next);

    screen.add_layer(abort);
}

pub fn show_info_window<F>(screen: &mut Cursive, msg: &str, cb_next: F)
where
    F: 'static + Fn(&mut Cursive),
{
    let abort = Dialog::around(TextView::new(msg))
    .title("Info")
    .button("Ok", cb_next);

    screen.add_layer(abort);
}