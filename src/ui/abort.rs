use cursive::views::{Dialog, TextView};
use cursive::Cursive;

use crate::ui::poweroff;

pub fn show_abort_window(screen: &mut Cursive) {
    let abort = Dialog::around(TextView::new(
        "Are you sure you want to abort the installation\nand exit?",
    ))
    .title("Abort")
    .button("No I don't want", on_cancel)
    .button("Shut down PC", poweroff::poweroff_window);

    screen.add_layer(abort);
}

fn on_cancel(screen: &mut Cursive) {
    screen.pop_layer();
    super::welcome::show_welcome_window(screen);
}
