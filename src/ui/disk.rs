use cursive::align::HAlign;
use cursive::event::EventResult;
use cursive::views::{Dialog, LinearLayout, OnEventView, Panel, SelectView, TextView};
use cursive::Cursive;

use crate::blockdevices::{get_disks, Disks};
use crate::settings::Settings;
use crate::ui::error::show_error_window;

pub fn show_disk_window(screen: &mut Cursive) {
    let mut disk_select = SelectView::new()
        // Center the text horizontally
        .h_align(HAlign::Center)
        // Use keyboard to jump to the pressed letters
        .autojump();

    disk_select.set_on_submit(on_disk_selected);

    // Detect blockdevices
    let disks = match get_disks() {
        Ok(disks) => disks,
        Err(why) => {
            let msg = format!("Cannot find block devices.\n{}", why);
            log::error!("{}", msg);
            show_error_window(screen, &msg);
            Disks::default()
        }
    };

    let devices = disks.list();
    for disk in devices {
        disk_select.add_item(disk.to_string(), disk.name.unwrap_or_default().to_string());
    }

    // Let's override the `j` and `k` keys for navigation
    let disk_select = OnEventView::new(disk_select)
        .on_pre_event_inner('k', |s, _| {
            let cb = s.select_up(1);
            Some(EventResult::Consumed(Some(cb)))
        })
        .on_pre_event_inner('j', |s, _| {
            let cb = s.select_down(1);
            Some(EventResult::Consumed(Some(cb)))
        });

    screen.add_layer(
        Dialog::around(
            LinearLayout::vertical()
                .child(TextView::new(
                    "Select the disk where you want to install the system:",
                ))
                .child(Panel::new(disk_select)),
        )
        .title("Select master system disk")
        .button("Back", on_back),
    );
}

fn on_disk_selected(screen: &mut Cursive, selected: &str) {
    screen.pop_layer();

    screen.with_user_data(|settings: &mut Settings| {
        settings.disk = selected.to_string();
    });

    let dialog = Dialog::around(TextView::new(format!("You selected: {}", selected)))
        .title("Information")
        .button("Ok", on_ok)
        .button("Cancel", on_cancel);

    screen.add_layer(dialog);
}

fn on_cancel(screen: &mut Cursive) {
    screen.pop_layer();
    show_disk_window(screen);
}

fn on_ok(screen: &mut Cursive) {
    screen.pop_layer();

    log::info!(
        "Selected disk: {}",
        screen
            .user_data::<Settings>()
            .unwrap_or(&mut Settings::new())
            .disk
    );

    super::user_management::user_manager(screen);
}

fn on_back(screen: &mut Cursive) {
    screen.pop_layer();

    super::hostname::show_hostname_window(screen);
}
