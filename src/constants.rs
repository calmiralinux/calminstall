pub const REQUIRED_CPU: i32 = 800; // Minimum 800 Mhz
pub const REQUIRED_MEM: u64 = 64; // Minimum 64 Mb RAM

pub const LICENSE_PATH: &str = "./LICENSE";
pub const FLIST_PATH: &str = "./flist";
pub const LOG_PATH: &str = "/tmp/calminstall.log";
pub const TIMEZONE_DIR: &str = "/usr/share/zoneinfo";

pub const DEFAULT_HOSTNAME: &str = "calmira";
