use std::{fmt, process::Command};

use anyhow::Result;
use serde::Deserialize;

#[derive(Debug, Deserialize, Clone, Default)]
pub struct Disk {
    pub name: Option<String>,
    pub size: Option<String>,
    pub model: Option<String>,
}

impl fmt::Display for Disk {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "<{}>: {} {}",
            self.name.as_ref().unwrap_or(&String::new()),
            self.size.as_ref().unwrap_or(&String::new()),
            self.model.as_ref().unwrap_or(&String::new())
        )
    }
}

#[derive(Debug, Deserialize, Default)]
pub struct Disks {
    blockdevices: Option<Vec<Disk>>,
}

impl Disks {
    pub fn list(&self) -> Vec<Disk> {
        let mut list: Vec<Disk> = Vec::new();
        if let Some(blockdevices) = &self.blockdevices {
            for disk in blockdevices {
                list.push(disk.clone());
            }
        } else {
            log::warn!("Cannot find block devices.");
        }

        list
    }
}

// impl Default for Disks {
//     fn default() -> Self {
//         Self { blockdevices: None }
//     }
// }

pub fn get_disks() -> Result<Disks> {
    let output = Command::new("lsblk")
        .arg("-Jdno")
        .arg("name,size,model")
        .output()?;
    let disks = String::from_utf8_lossy(&output.stdout);
    Ok(serde_json::from_str(&disks)?)
}
