use core::fmt;

use anyhow::{anyhow, Result};

use crate::constants;

#[derive(Debug)]
pub struct Settings {
    pub hostname: String,
    pub disk: String,
    pub tz: String,
    pub users: Vec<UsersSettings>,
}

impl Settings {
    pub fn new() -> Self {
        Self {
            hostname: constants::DEFAULT_HOSTNAME.to_string(),
            disk: String::new(),
            users: Vec::new(),
            tz: String::new(),
        }
    }

    pub fn get_all_uids(&self) -> Vec<String> {
        let mut uids = Vec::new();
        for user in self.users.iter() {
            if user.uid.is_some() {
                uids.push(user.uid.clone().unwrap_or_default());
            }
        }

        uids
    }

    pub fn get_all_gids(&self) -> Vec<String> {
        let mut gids = Vec::new();
        for user in self.users.iter() {
            if user.gid.is_some() {
                gids.push(user.gid.clone().unwrap_or_default());
            }
        }

        gids
    }

    /// Tries to find and remove an user
    pub fn remove_user(&mut self, user: &UsersSettings) -> Result<()> {
        for (index, existing_user) in self.users.iter().enumerate() {
            if user == existing_user {
                self.users.remove(index);
                return Ok(());
            }
        }

        Err(anyhow!("Can't remove! User {} not found.", user.name))
    }
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            hostname: constants::DEFAULT_HOSTNAME.to_string(),
            disk: String::new(),
            users: Vec::new(),
            tz: String::new(),
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct UsersSettings {
    /// User name
    ///
    /// In window: `User name (short)`
    pub name: String,
    /// User description (full name)
    ///
    /// In window: `User name (full)`
    pub user_description: Option<String>,
    /// User password
    ///
    /// In window: `Password`
    pub password: String,
    /// Whether to create a group for this user
    ///
    /// In window: checkbox `Create user group`
    pub user_group: bool,
    /// Custom UID
    ///
    /// In window: `Custom User ID`
    pub uid: Option<String>,
    /// Custom GID
    ///
    /// In window: `Custom Group ID` (only if `user_group` enabled (`true`)!)
    pub gid: Option<String>,
    /// Login shell
    ///
    /// In window: `Custom Login shell`
    /// Default value: `String::from("/bin/bash")`
    pub login_shell: String,
    /// Home directory
    ///
    /// In window: `Custom Home directory`
    /// Default value: `format!("/home/{}", &name)`
    pub home_dir: Option<String>,
    /// Is the account a system account?
    ///
    /// In the window: `This is system account`
    pub system_account: AccountType,
}

/// Format fileds.
impl fmt::Display for UsersSettings {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f,
            "User: {}; UID: {}; GID: {}\nDescription: {}\nPassword: {}\nHome: /home/{}; Shell: {}\nSystem account: {}",
            self.name.clone(),
            self.uid.clone().unwrap_or_else(|| "Unknown".to_string()),
            self.gid.clone().unwrap_or_else(|| "Unknown".to_string()),
            self.user_description.clone().unwrap_or_else(|| "No description".to_string()),
            self.password.clone(),
            self.home_dir.clone().unwrap_or_else(|| "Not provided".to_string()),
            self.login_shell.clone(),
            self.system_account,
        )
    }
}

impl PartialEq for UsersSettings {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl UsersSettings {
    /// Check  if required fields are filled correctly
    pub fn is_correct(&self) -> bool {
        let mut result = true;

        if self.name.is_empty() {
            result = false;
        }

        if self.password.is_empty() {
            result = false;
        }

        result
    }
}

#[derive(Debug, Clone, Default)]
pub struct AccountType(pub bool);

impl AccountType {
    pub fn new(value: bool) -> Self {
        Self(value)
    }
}

impl fmt::Display for AccountType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.0 {
            write!(f, "System")
        } else {
            write!(f, "Simple")
        }
    }
}
