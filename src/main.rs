mod blockdevices;
mod constants;
mod settings;
mod ui;
mod utils;

use log::LevelFilter;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Config, Root};
use log4rs::encode::pattern::PatternEncoder;

use settings::Settings;
use ui::welcome::show_welcome_window;

fn main() {
    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{l} - {m}\n")))
        .build(constants::LOG_PATH)
        .unwrap();

    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .build(Root::builder().appender("logfile").build(LevelFilter::Info))
        .unwrap();

    log4rs::init_config(config).unwrap();
    let mut screen = cursive::default();

    screen.set_user_data(Settings::new());
    show_welcome_window(&mut screen);

    screen.run();
}
