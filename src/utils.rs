use std::fs;
use std::path::Path;
use std::process::{exit, Command, Stdio};

use anyhow::{anyhow, Result};
use cursive::utils::Counter;
use walkdir::{DirEntry, WalkDir};

#[allow(unused)]
pub enum ScanType {
    Dir,
    File,
    DirAndFile,
}

fn run_cmd(cmd: &str, args: &Vec<&str>) -> Result<i32> {
    let output = Command::new(cmd)
        .args(args)
        .stdout(Stdio::piped())
        .output()?;

    if !output.status.success() {
        return Err(anyhow!("{}", String::from_utf8(output.stderr)?));
    }

    Ok(output.status.code().unwrap_or_default())
}

pub fn poweroff() {
    match run_cmd("poweroff", &Vec::new()) {
        Ok(_) => exit(0),
        Err(why) => {
            eprintln!("{}", why);
            exit(1);
        }
    }
}

pub fn restart() {
    match run_cmd("reboot", &Vec::new()) {
        Ok(_) => exit(0),
        Err(why) => {
            eprintln!("{}", why);
            exit(1);
        }
    }
}

pub fn get_cpu_freq_mhz() -> i32 {
    let mut max_freq = 0;
    let cpus = cpu_freq::get();

    // Choose fastest core. Can be usefull for ARM/Risk CPUs
    for cpu in cpus {
        let current_cpu_freq = cpu.max.unwrap_or(0.0);
        if max_freq < current_cpu_freq as i32 {
            max_freq = current_cpu_freq as i32;
        }
    }

    max_freq
}

pub fn get_memory_mb() -> u64 {
    let content = match fs::read_to_string("/proc/meminfo") {
        Ok(content) => content,
        Err(why) => {
            log::warn!("Can't open /proc/meminfo: {}", why);
            return 0;
        }
    };

    let memory = content
        .lines()
        .filter(|line| line.contains("MemTotal"))
        .collect::<Vec<_>>();

    if memory.is_empty() {
        return 0;
    }

    let parts: Vec<&str> = memory[0].splitn(2, ':').collect();

    match parts[1].trim_start().split(' ').next() {
        Some(kb) => {
            if let Ok(value) = kb.parse::<u64>() {
                value / 1024
            } else {
                log::warn!("Can not find memory value.");
                0
            }
        }
        None => 0,
    }
}

/// Receive list of files and check if file exists
///
/// Return true if at least one file not exist.
pub fn check_files(files: Vec<String>, counter: &Counter) -> bool {
    let mut is_err = false;
    for file in files {
        if !Path::new(&file).is_file() && !Path::new(&file).is_symlink() {
            is_err = true;
            log::error!("File {} not found!", file);
            break;
        }

        counter.tick(1);
    }

    is_err
}

pub fn get_shells() -> Vec<String> {
    let content = match fs::read_to_string("/etc/shells") {
        Ok(content) => content,
        Err(why) => {
            log::warn!("Can't open /etc/shells: {}", why);
            "/bin/sh\n/bin/bash\n".to_owned()
        }
    };

    content
        .lines()
        .filter(|s| !s.contains('#'))
        .filter(|s| !s.is_empty())
        .map(|s| s.to_string())
        .collect()
}

pub fn get_shell_index(shells: &[String], shell: &str) -> Option<usize> {
    for (i, sh) in shells.iter().enumerate() {
        if *sh == shell {
            log::info!("Shell {} found and has index {}", shell, i);
            return Some(i);
        }
    }
    log::info!("Shells {} not found", shell);
    None
}

/// Search max id and return value max_id + 1
pub fn calculate_id(ids: &Vec<String>) -> String {
    let mut new_id = 1000;

    if ids.is_empty() {
        return new_id.to_string();
    }

    for id in ids {
        let id_as_num = id.parse::<i32>().unwrap_or(1000);

        // Find max used uid
        if id_as_num > new_id {
            new_id = id_as_num;
        }
    }

    new_id += 1;

    new_id.to_string()
}

/// Scan given directory.
///
/// Params:
///     dir: directory to scan
///     depth: recursion depth
///     scan_type: what should be returned
pub fn scan_dir(dir: &Path, depth: usize, scan_type: ScanType) -> Result<Vec<String>> {
    let mut dir_scan: Vec<String> = Vec::new();

    log::debug!("Dir to scan: {}", dir.display());
    if !Path::new(dir).is_dir() {
        return Err(anyhow!(
            "Directory {} does not exist",
            dir.to_string_lossy()
        ));
    }

    // Check scan depth
    let scan_depth = if depth >= 1 { depth } else { 1 };

    // Receive iterator over all items in the path
    let walker = WalkDir::new(dir)
        .min_depth(1)
        .max_depth(scan_depth)
        .into_iter();
    // Iterate over and ignore all errors
    for entry in walker
        .filter_map(|e| e.ok())
        .filter(|e| (!is_hidden(e) && is_type(e, &scan_type)))
    {
        dir_scan.push(trim_path(entry.path()));
    }

    Ok(dir_scan)
}

/// Return final component of the path or empty string if there is no component
fn trim_path(path: &Path) -> String {
    let file = path.file_name().unwrap_or_default();

    let fname = file.to_str().unwrap_or("");

    String::from(fname)
}

/// Check if the entry is provided type
fn is_type(entry: &DirEntry, scan_type: &ScanType) -> bool {
    match scan_type {
        ScanType::Dir => entry.path().is_dir(),
        ScanType::File => entry.path().is_file(),
        ScanType::DirAndFile => entry.path().is_dir() || entry.path().is_file(),
    }
}

/// Check if the entry is hidden
fn is_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with('.'))
        .unwrap_or(false)
}
